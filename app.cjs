const express = require('express');
const app = express();
const { port } = require('./config');
const uuid = require('crypto');
const http = require('http');
const fs = require('fs');
const path = require('path');
const { error } = require('console');
const reqestId = require('express-request-id');
app.use(reqestId());

let logFilePath = path.join(__dirname,'./requests.log');
function createLogsFile(request,response,next){
    let data = `Request Id : ${request.id}, Request Method : ${request.method}, Request URL : ${request.url} \n`;
    fs.appendFile(logFilePath, data,(error)=>{
        if(error){
            next({
                message : 'Error while fetching requests',
                status : 404
            });
            console.error(error);
        }
    });
    next();
}
app.use(createLogsFile);

app.get('/html', (reqest, response) => {
    response.send(`<!DOCTYPE html>
    <html>
      <head>
      </head>
      <body>
          <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
          <p> - Martin Fowler</p>
      </body>
    </html>`);
});

app.get('/json', (reqest, response) => {
    response.json(
        {
            "slideshow": {
                "author": "Yours Truly",
                "date": "date of publication",
                "slides": [
                    {
                        "title": "Wake up to WonderWidgets!",
                        "type": "all"
                    },
                    {
                        "items": [
                            "Why <em>WonderWidgets</em> are great",
                            "Who <em>buys</em> WonderWidgets"
                        ],
                        "title": "Overview",
                        "type": "all"
                    }
                ],
                "title": "Sample Slide Show"
            }
        });
});

app.get('/uuid', (reqest, response) => {
    response.send({
        'uuid': uuid.randomUUID()
    });
});

app.get('/status/:status', (request, response,next) => {
    const { status } = request.params;
    const statusCode = http.STATUS_CODES[status];

    if (statusCode === undefined || status === undefined) {
        // response.statusCode = 404;
        next({
            message : 'Check Your Status Code its Invalid',
            status : 404
        });
    } else {
        response.status(status)
        response.send({
            'Status code': status
        });
    }
});

app.get('/delay/:time', (request, response,next) => {
    time = request.params.time;
    if (time >= 0) {
        setTimeout(() => {
            response.json({
                '200': 'OK'
            });
        }, time * 1000);
    }
    else {
        next({
            message: 'Invalid delay time',
            status : 400
        });
    }
});

app.get('/', (request, response) => {
    response.send(`
    <!DOCTYPE html>
        <html>
          <head>
          </head>
          <body></body><h1>Welcome to Home Page</h1>
    <h3>try these valid EndPoint :-</h3>
    <h4>/html</h4>
    <h4>/json</h4>
    <h4>/uuid</h4>
    <h4>/logs</h4>
    <h4>/status/statusCode</h4>
    <h4>/delay/timeInSecond</h4>
    </body>
      </html>`);
});
app.get('/logs', (request,response)=>{
    response.sendFile(logFilePath);
});

app.use((request, response) => {
    response.status(404).send({
        'message': 'Enter valid route'
    });
});

app.use((error,request,resolve,next)=>{
    console.error('error ', error);
    resolve.status(error.status).json({
        error : error.message
    });
});
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});